const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')


const FollowSchema = new Schema({
    follower : {type : Schema.Types.ObjectId , required : true , res : 'User'},
    followed : {type : Schema.Types.ObjectId , required : true , res : 'User'},
    status : {type : Boolean , required : true}
})


FollowSchema.plugin(timeStamp)
module.exports = mongoose.model('Follow',FollowSchema); 