const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')

const PrivateRoomSchema = new Schema({
    roomID : {type : String , unique : true ,require : true , index : true},
    chatWith : [{type : String , require : true , index : true , ref : 'User'}]
})

PrivateRoomSchema.plugin(timeStamp)


module.exports = mongoose.model('PrivateRoom',PrivateRoomSchema);