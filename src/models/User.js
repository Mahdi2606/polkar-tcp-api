const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp');
const bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const userSchema = new Schema({

    "name" : { type : String , required : true },

    "username" : { type : String , required : true , unique : true , index : true},

    "password" : { type : String , required : true },

    "phone" : { type : Number, required : true , unique: true , index : true },

    "family" : { type : String , default : null},

    "email" : { type : String , default : null , index : true},

    "likes" : { type : Number , default : 0},

    "views" : { type : Number , default : 0},

    "point" : { type : Number , default : 0},

    "type" : { type : String , default : "user" },

    "profile" : { type : String , default : 'user.png'},

    "posts" : [{ type : Schema.Types.ObjectId , ref : 'Post'}],

    "follower" : [{type : Schema.Types.ObjectId , ref : 'User'}],

    "following" : [{type : Schema.Types.ObjectId , ref : 'User'}],

    "settings" : {type : Schema.Types.ObjectId , ref : 'UserSetting' , required : true},

})

userSchema.plugin(timeStamp);
// userSchema.pre('save',function(next){
//     bcrypt.hash(`${this.password}`, 10 , (err , hash)=>{
//         this.password = hash;
//         next();
//     });
// });


module.exports =  mongoose.model('User' , userSchema);