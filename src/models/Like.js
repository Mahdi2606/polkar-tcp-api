const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')


const LikeSchema = new Schema({
    liker : {type : Schema.Types.ObjectId , required : true , res : 'User' , index : true},
    post : {type : Schema.Types.ObjectId , required : true , res : 'Post' , index : true},
})


LikeSchema.plugin(timeStamp)
module.exports = mongoose.model('Like',LikeSchema); 