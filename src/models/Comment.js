const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')
const mongoosePaginate = require('mongoose-paginate');


const CommentSchema = new Schema({
    post : {type : Schema.Types.ObjectId , required : true , ref : 'Post' , index : true},
    user : {type : Schema.Types.ObjectId , required : true , ref : 'User', index : true},
    text : {type : String , required : true},
})

CommentSchema.plugin(timeStamp)
CommentSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('Comment',CommentSchema);