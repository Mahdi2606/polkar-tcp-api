const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')
const mongoosePaginate = require('mongoose-paginate');

const PostSchema = new Schema({
    subject : {type : String},
    text : {type : String , default : ''},
    view : {type : Number , default : 0},
    like : {type : Number , default : 0},
    commentCount : {type : Number , default : 0},
    user : {type : Schema.Types.ObjectId , ref : 'User' , index : true},
    photos : {type : Array , default : []},
    pin : {type : Boolean , default : false},
    commentMode : {type : Boolean , default : false},
    likeMode : {type : Boolean , default : false},
    hashtag : {type : String}
})

PostSchema.plugin(timeStamp)
PostSchema.plugin(mongoosePaginate)


module.exports = mongoose.model('Post',PostSchema);