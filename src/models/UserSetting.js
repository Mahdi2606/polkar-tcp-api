const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timeStamp = require('mongoose-timestamp')


const UserSettingSchema = new Schema({
    user : {type : Schema.Types.ObjectId , required : true , unique : true , ref : 'User' , index : true},
    pageMode : {type : Boolean , default : false},
})

UserSettingSchema.plugin(timeStamp)


module.exports = mongoose.model('UserSetting',UserSettingSchema);   