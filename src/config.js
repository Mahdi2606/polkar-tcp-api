
const path = require('path');

module.exports =  {
    port : 2000,
    jwt : 'Classic!@#$%^&net!@#$%^2326',
    path : {
        model : path.resolve('./src/models'),
        controller : path.resolve('./src/controllers'),
        middleware : path.resolve('./src/middlewares'),
        transforms : path.resolve('./src/transforms'),
    }
};


