


const transform = require('./../transform');
const jwt = require('jsonwebtoken');

module.exports = class loginTransform extends transform{

    transform(item){
        return {
            name : item.name,
            username  : item.username,
            ...this.token(item._id),
        }
    }

    token(item){
        let token = jwt.sign({ id : item._id }, config.jwt,{
            expiresIn : '200h'
        });
        return {
            token,
        }
    }

}