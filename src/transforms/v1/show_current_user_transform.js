

const transform = require('./../transform');


module.exports = class showCurrentUserTransform extends transform{

    transform(item){
        return {
          family : item.family,
          email : item.email,
          likes : item.likes,
          views : item.views,
          point : item.point,
          profile : item.profile,
          name : item.name,
          username : item.username,
          phone : item.phone,
          follower : item.follower.length,
          following : item.following.length,
          settings : {
            pageMode : item.settings.pageMode  
          } 
        }
    }

}
