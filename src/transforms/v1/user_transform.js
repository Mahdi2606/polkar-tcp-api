

const transform = require('./../transform');


module.exports = class userTransform extends transform{

    transform(item , withPost){
        return {
          family : item.family,
          email : item.email,
          likes : item.likes,
          views : item.views,
          point : item.point,
          profile : item.profile,
          name : item.name,
          username : item.username,
          password : item.password,
          phone : item.phone,
          settings : {
            pageMode : item.settings.pageMode  
          } ,
          posts : withPost 
          ? 
          {
            posts : item.posts,
            status : true
          }
          : 
          {
            message : 'این صفحه خصوصی می باشد',
            status : false
          }
        }
    }

}
