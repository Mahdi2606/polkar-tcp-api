

const transform = require('./../transform');


module.exports = class giveUserTransform extends transform {

    transform(item , auth , follow , pageMode){
        return {
            family : item.family,
            email : item.email,
            likes : item.likes,
            views : item.views,
            point : item.point,
            profile : item.profile,
            name : item.name,
            username : item.username,
            follower : item.follower.length,
            following : item.following.length,
            // password : item.password,
            phone : item.phone,
            auth : auth,
            follow : follow,
            settings : {
                pageMode : item.settings.pageMode
            },
            // posts : (pageMode) 
            //         ?   (follow.request && follow.status)
            //             ?   {
            //                 posts : item.posts,
            //                 message : 'Everythisg is OK',
            //             } 
            //             :   {
            //                 posts : false,
            //                 message : 'This account is private'
            //             }
            //         :   {
            //             posts : item.posts,
            //             message : 'Everythisg is OK',
            //         }
        }
    }

}
