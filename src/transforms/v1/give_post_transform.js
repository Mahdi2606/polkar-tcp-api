

const transform = require('./../transform');


module.exports = class givePostTransform extends transform{

    transform(item){
        return {
          id : item._id,
          subject : item.subject,
          text : item.text,
          view : item.view,
          like : item.like,
          commentCount : item.commentCount,
          photos : item.photos,
          hashtag : item.hashtag,
          pin : item.pin,
          commentMode : item.commentMode,
          likeMode : item.likeMode,
          user : {
            username : item.user.username,
            profile : item.user.profile,
          }

        }
    }

}
