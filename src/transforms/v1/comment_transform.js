

const transform = require('./../transform');


module.exports = class commentTransform extends transform{

    transform(item){
        return {
          username : item.user.username,
          profile : item.user.profile,
          text : item.text,
          createdAt : item.createdAt
        }
    }
}