

const transform = require('./../transform');


module.exports = class postTransform extends transform{

    transform(item , data){
        console.log(data)
        return {
          id : item._id,
          subject : item.subject,
          text : item.text,
          view : item.view,
          like : item.like,
          commentCount : item.commentCount,
          photos : item.photos,
          hashtag : item.hashtag,
          pin : item.pin,
          commentMode : item.commentMode,
          likeMode : item.likeMode,
          user : {
            username : item.user.username,
            profile : item.user.profile,
          }

        }
    }

}
