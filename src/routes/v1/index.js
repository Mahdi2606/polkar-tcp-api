const express = require('express');
const route = express.Router();

//controllers
const authController = require('./../../controllers/v1/auth_controller');
const userController = require('./../../controllers/v1/user_controller');
const postController = require('./../../controllers/v1/post_controller');
const followController = require('./../../controllers/v1/follow_controller');
const commentController = require('./../../controllers/v1/comment_controller');
const likeController = require('./../../controllers/v1/like_controller');
//middleware
const authMiddleware = require('./../../middlewares/auth_middleware');
const pageModeMiddleware = require('./../../middlewares/page_mode_middleware');
const checkFollowMiddleware = require('./../../middlewares/check_follow_middleware');
const updateMiddleware = require('./../../middlewares/body_checker/update_middleware');
const userPageModeMiddleware = require('./../../middlewares/user_page_mode_middleware');
const {uploader,avatarUploader} = require('./../../middlewares/appUploadMiddleware');

//initial- route
route.get('/' , (req , res) => {
    
    res.json(
        {
            message : 'Api version 1' , 
            status : true
        }
    )
}
)

//register 
route.post('/register' , authController.register.bind(authController));

//login
route.post('/login' , authController.login.bind(authController));

//get-user-with-token
route.get('/user' , authMiddleware , userController.show_current_user.bind(userController));

//get-user-information
// route.get('/user/:username' , userController.getUser.bind(userController))

//getUser new V
route.get('/user/:username' , userPageModeMiddleware , authMiddleware , checkFollowMiddleware , userController.giveUser.bind(userController))

//update-user
route.post('/user' , authMiddleware , updateMiddleware , avatarUploader , userController.updateUser.bind(userController))

//add-new-post
route.post('/post' , authMiddleware , uploader , postController.addPost.bind(postController)); //Remember : Add middleware before sending, create and save photos and set the photo path in req.postPhoto and use it when creating a post.

//get-auth-user-posts
route.get('/post' , authMiddleware , postController.getAuthUserPost.bind(postController))

//get-user-posts-by-username
route.get('/posts/:username' , userPageModeMiddleware , authMiddleware , checkFollowMiddleware , postController.getUsersPosts.bind(postController))

//get-one-post
route.get('/post/:id' , pageModeMiddleware , authMiddleware , checkFollowMiddleware , postController.showPost.bind(postController))

//follow
route.get('/follow/:username' , authMiddleware , followController.follow.bind(followController))

//unfollow
route.delete('/follow/:username' , authMiddleware , followController.unfollow.bind(followController))

//set-comment
route.post('/comment/:id' , authMiddleware , commentController.addComment.bind(commentController))

//get-comment
route.get('/comment/:id' , pageModeMiddleware , authMiddleware , checkFollowMiddleware , commentController.showPostComment.bind(commentController))

//get-last-comment
route.get('/comment/last/:id' , authMiddleware , commentController.getLastComment.bind(commentController))

//setLike
route.post('/like/:id' , authMiddleware , likeController.setLike.bind(likeController));

//checlLike
route.get('/like/:id' , authMiddleware , likeController.checkLike.bind(likeController));

//show-following-user-posts
route.get('/getposts' , authMiddleware , postController.getFollowingUserPosts.bind(postController))

//search-user
route.get('/user/search/:username' , authMiddleware , userController.search.bind(userController))

//search-post-by-hashtag
route.get('/search/hashtag/:hashtag' , authMiddleware , postController.searchByHashtag.bind(postController))

//search-post-by-text
// route.get('/search/text/:text' , authMiddleware , postController.search)

//text-route
route.post('/test' , (req , res)=>{
    let data = req.body;
    // let json = JSON.parse(images)
    console.log(data)
})

module.exports = route;
