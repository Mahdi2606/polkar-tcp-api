const postGetter = require('./../modules/getter/v1/post_getter');
const ObjectId = require('mongoose').Types.ObjectId;
const userGetter = require('./../modules/getter/v1/user_getter');
const postTransform = require('./../transforms/v1/posts_transform')
module.exports = async (req , res , next) => {

  let postID = req.params.id
  let idValidate = await ObjectId.isValid(postID)

  if(!idValidate){
    
    res.json({
      message : 'ایدی مطلب معتبر نیست',
      status : false
    })
    return
  }else{
    let post = await postGetter.get_post_by_id(postID)
    if(post === null){
      res.json({
        message : 'ایدی مطلب معتبر نیست',
        status : false
      })
      return
    }else{
      let result = new postTransform().transform(post)
      let userID = await userGetter.get_user_id_by_username(result.user.username)
      let settings = await userGetter.get_user_setting_by_id(userID);
      let pageMode =  settings.pageMode;
      let privateUserID = settings.user
      req.privateUserID = userID
      req.authRequired = 'optional';
      req.followRequired = 'optional';
      next()
    }
  }

}
