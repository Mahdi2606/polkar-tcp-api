
const UserGetter = require('../modules/getter/v1/user_getter');
const socketUserTransform = require('../socket/transforms/sokcet_user_transform')
module.exports = async (req , res , next) => {
  let authRequired = req.authRequired
  switch (authRequired) {
    case 'necessary':
    Necessary(req , res , next)
    break;
    case 'optional':
    Optional(req , res , next)
    break;
    // case 'unnecessary':
    //   req.user = null;
    //   next();
    // break;  
    default:
      Necessary(req , res , next)
    break;
  }
}



let Necessary = async (req , res , next) => {
  let token = req.body.token || req.query.token || req.headers['x-api-key'];
  let user = await UserGetter.get_user_with_token(token)
  if(user === null){
    res.json({
      message : 'کاربر احراز هویت نشد',
      status : false
    })
    return
  }else{

    req.user = user;
    req.user.token = token;
    next()
  }
}

let Optional = async (req , res , next) => {
  let token = req.body.token || req.query.token || req.headers['x-api-key'];
  let user = await UserGetter.get_user_with_token(token)
  if(user === null){
    req.user = null
    next()
  }else{
    req.user = user;
    req.user.token = token;
    next()
  }
}
