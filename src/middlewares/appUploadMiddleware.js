
const jwt = require('jsonwebtoken');
const fs = require('fs');
const mkdirp = require('mkdirp');
const { promisify } = require('util')

const writeFile = promisify(fs.writeFile)

const uploader = async(req , res , next)=>{



    let date = Date.now();
    let dir = './files/'
    mkdirp(dir,(err)=>{
        if(err){
            console.log(err)
        }
    })
    
    let id = await jwt.verify(req.user.token , 'Classic!@#$%^&net!@#$%^2326', (err, decoded)=> decoded.id )
    let medias = req.body.medias;
    let file = medias.map(media => {
        let name = `${date}-${id}-${media.name}`
        writeFile(`${dir}${name}`, Buffer.from(media.data,'base64'))
        return name
    }
    );

    req.medias = file
    next()
}

const avatarUploader = async(req , res , next)=>{
    if(req.body.avatar){
        let date = Date.now();
        let dir = './files/avatar/'
        mkdirp(dir,(err)=>{
            if(err){
                console.log(err)
            }
        })
        let id = await jwt.verify(req.user.token , 'Classic!@#$%^&net!@#$%^2326', (err, decoded)=> decoded.id )
        let avatar = req.body.avatar;
        let name = `${date}-${id}-${avatar.name}`
        writeFile(`${dir}${name}`, Buffer.from(avatar.data,'base64'))
        req.avatar = name
        next()
    }else{
        req.avatar = null;
        next()
    }
}

module.exports = {
    uploader,
    avatarUploader
}