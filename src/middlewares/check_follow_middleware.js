
const FollowGetter = require('./../modules/getter/v1/follow_getter');
const postTransform = require('./../transforms/v1/posts_transform');
module.exports = async (req, res, next) => {
  let followRequired = req.user ? req.followRequired : 'unnecessary';

  switch (followRequired) {
    case 'necessary':
      Necessary(req, res, next)
      break;
    case 'optional':
      Optional(req, res, next)
      break;
    case 'unnecessary':
      req.follow = false
      next()
      break;
    default:
      Necessary(req, res, next)
      break;
  }

}


let Necessary = async (req, res, next) => {

  let followerID = req.user._id;
  let followedID = req.privateUserID;
  let follow = await FollowGetter.check_follow_exist_and_accept(followerID, followedID)
  if (follow === null || !follow.status) {
    res.json({
      message: 'شما اجازه مشاهده این صفحه را ندارید',
      status: false
    })
    return
  } else {
    req.follow = true
    next();
  }
}

let Optional = async (req, res, next) => {
  if (req.user) {
    let followerID = req.user._id;
    let followedID = req.privateUserID;
    let follow = await FollowGetter.check_follow_exist_and_accept(followerID, followedID)
    if (follow === null) {
      req.follow = {
        request : false,
        status : false
      };
      next();
    } else {
      if(follow.status){
        req.follow = {
          request : true,
          status : true
        }
        next();
      }else{
        req.follow = {
          request : true,
          status : false
        }
        next();
      }
    }
  }else{
    req.follow = {
      request : false,
      status : false
    };
    req.auth = false;
    next();
  }
}
