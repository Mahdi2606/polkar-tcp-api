

module.exports = async (req , res , next) => {
  let newUpdate = []
  let notValid = []
  let updateField = ['username' , 'email' , 'name' , 'family' , 'avatar']
  let update = req.body
  let keys = Object.keys(update);
  let values = Object.values(update);
  await keys.map((key , index) => {
    if(updateField.includes(key)){
      newUpdate[key] = values[index]
    }else{
      notValid[key] = values[index]
    }
  })
  req.body = await Object.assign({} , newUpdate)
  req.notValid = await Object.assign({} , notValid)
  next()
}
