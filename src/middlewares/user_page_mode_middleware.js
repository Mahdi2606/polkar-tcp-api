const userGetter = require('./../modules/getter/v1/user_getter');

module.exports = async (req , res , next) => {
 let userID = await userGetter.get_user_id_by_username(req.params.username);
 if(userID === null) {
   res.json({
     message : 'کاربری با این نام کاربری یافت نشد',
     status : false
   })
   return
 }else{
    let settings = await userGetter.get_user_setting_by_id(userID);
    req.pageMode = settings.pageMode;
    req.privateUserID = userID;
    req.authRequired = 'optional';
    req.followRequired = 'optional';
    next();
 }
}