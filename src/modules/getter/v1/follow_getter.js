
const controller = require('../../../controllers/controller');

module.exports = new class FollowGetter extends controller {


  async check_follow_exist(followerID , followedID){
    let exist = await this.model.Follow.findOne({follower : followerID , followed : followedID} , (err , result)=>{
      if(err || result === null){
        return null;
      }else{
        return true;
      }
    })
    return exist;
  }

  async check_follow_exist_and_accept(followerID , followedID){
    let exist = await this.model.Follow.findOne({follower : followerID , followed : followedID} , {_id : 0 , status : 1} , (err , result)=>{
      if(err || result === null){
        return null;
      }else{
        return result;
      }
    })

    return exist;
  }


}