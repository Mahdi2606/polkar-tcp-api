

const controller = require('../../../controllers/controller');
const jwt = require('jsonwebtoken');

module.exports = new class UserGetter extends controller {
  
  async get_user(username , password){
    let user = await this.model.User.findOne({username : username , password : password} , (err , user)=>{
      if(err || user === null){
        return null
      }else{
        return user
      }
    });
    return user
  }

  async check_user_exist(object){
    let user = await this.model.User.findOne(object , (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    })
    return user
  }

  async get_user_id_by_username(username){
    let id = await this.model.User.findOne({username : username} , {_id : 1} , (err , result) => {
      if(err || result === null){
        return null
      }else{
        return result
      }
    })
    let userID = id === null ? id : id._id;
    return userID
  }

  async get_user_by_id(id){
    let userData = await this.model.User.findById(id , (err , user)=>{
      if(err){
        return null
      }else{
        return user
      }
    }).populate('settings');
    return userData

  }

  async get_user_with_token(token){
    let decodedUser = await jwt.verify(token, config.jwt , async (err, decoded) => {
      if(err){
        return null
      }else{
        let user = await this.get_user_by_id(decoded.id)
        if(user === null){
          return null
        }else{
          return user
        }
      }
    })
    return decodedUser
  }

  async get_user_id_by_post_id(postID){
    let userID = await this.model.Post.findById(postID , {user : 1} ,  (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result;
      }
    });

    return userID.user;

  }

  // async get_user_setting_by_username(username){
  //   let settings = await this.model.User.findOne({username : username} ,{settings : 1 , _id : 0} ,async (err , result)=>{
  //     if(err || result === null){
  //       return null
  //     }else{
  //       return result
  //     }

  //   }).populate('settings')
    
  //   return settings
  // }

  async get_user_setting_by_id(id){
    let settings = await this.model.User.findById(id  , {settings : 1 , _id : 0} ,async (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }

    }).populate('settings')
    
    let setting = settings === null ? settings : settings.settings
    return setting
  }

  async search_users(key){
    let string = key.trim();
    let lower = string.toLowerCase();
    let upper = string.toUpperCase();
    let regex = new RegExp(`\\b(${lower}|${upper}){1}`)
    let users = await this.model.User.find({username : { $regex: regex , $options: 'g' }} , {_id : 0 , username : 1 , profile : 1},(err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    })

    return users

  }

  
}
