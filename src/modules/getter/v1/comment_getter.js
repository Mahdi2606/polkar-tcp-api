

const controller = require('././../../../controllers/controller');

module.exports = new class CommentGetter extends controller {


  async get_post_comments(postID,page){
    let comments = await this.model.Comment.paginate(
      {post : postID} , 
      {limit : 20 , populate : ['user'] , page : page , sort : {createdAt : -1}} , 
      (err ,result) => {
        if(err){
          return null
        }else{
          return result
        }
    })
    return comments
  }

  async getLastComment(postID , userID){
    let comments = await this.model.Comment.findOne({post : postID , user : userID} , (err , result)=>{
      if(err){
        return null
      }else{
        return result;
      }
    }).sort({
      createdAt: -1
    }).populate('user')


    return comments


  }

}