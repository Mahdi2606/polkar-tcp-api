const controller = require('./../../../controllers/controller');

module.exports = new class PostGetter extends controller {

  //get_auth_user_posts
  async get_post_by_user_id(page,userID){
    try {
      let posts = this.model.Post.paginate({user : userID} , {page : page , limit : 6 , populate : ['user'] , sort : {createdAt : -1} } , (err , result)=>{
        if(err || result === null){
          return null;
        }else{
          return result;
        }
      })
      return posts;
    } catch (error) {
      console.log(error)
    }
  }


  async get_post_by_id(id){
    let post = this.model.Post.findById(id , (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    }).populate('user')
    return post;
  }


  async get_following_user_posts(following,page){

    let followingObjectId = await following.map((userID) => {
      return ObjectId(userID)
    })

    let posts = await this.model.Post.paginate({user : { $in : followingObjectId }} , {page : page , limit : 6 , populate : ['user'] , sort : {createdAt : -1}} , (err , result)=>{
      if (err || result === null) {
        return null
      }else{
        return result
      }
    })
    return posts

  }

  
  async search_posts_by_hashtag(key){
    let string = key.trim();
    let lower = string.toLowerCase();
    let upper = string.toUpperCase();
    let regex = new RegExp(`(#${lower}|#${upper})\\w*`)
    let posts = await this.model.Post.find({hashtag : { $regex: regex , $options: 'g' }} , {_id : 1},(err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    })

    return posts

  }


  


  // async search_in_post_text(key){
  //   let string = key.trim();
  //   let lower = string.toLowerCase();
  //   let upper = string.toUpperCase();
  //   let regex = new RegExp(`(#${lower}|#${upper})\\w*`)
  //   let posts = await this.model.Post.find({text : { $regex: regex , $options: 'g' }} , {_id : 1},(err , result)=>{
  //     if(err || result === null){
  //       return null
  //     }else{
  //       return result
  //     }
  //   })

  //   return posts

  // }


}
