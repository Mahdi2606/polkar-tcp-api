
const controller = require('./../../../controllers/controller');
const PostSetter = require('./../../../modules/setter/v1/post_setter')

module.exports = new class CommentSetter extends controller {


  async set_comment(postID , userID , text , cb){
    let setComment = this.model.Comment({
      post : postID,
      user : userID,
      text : text, 
    })

    let incrementComment = await PostSetter.incrementComment(postID);
    if(incrementComment === null){
      PostSetter.decrementComment(postID)
    }else{
      setComment.save(err => {
        if(err){
          cb(null)
        }else{
          cb(true)
        }
      })
    }
  }
}