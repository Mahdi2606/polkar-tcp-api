



const controller = require('./../../../controllers/controller');
const UserSetter = require('./user_setter');
const PostSetter = require('./post_setter');
const LikeGetter = require('./../../getter/v1/like_getter')

module.exports = new class LikeSetter extends controller {

    
    async set_like(postID , userID , cb) {
        let like = this.model.Like({
            'liker' : userID,
            'post' : postID,
        });
        let userLikeIncrement = await UserSetter.incrementLike(postID);
        let postLikeIncrement = await PostSetter.incrementLike(postID);
        
        if(userLikeIncrement === null && postLikeIncrement === null){
            cb(null)
        }else if(userLikeIncrement === null){
            await PostSetter.decrementLike(postID);
        }else if(postLikeIncrement === null){
            await UserSetter.decrementLike(postID);
        }else{
            like.save(async err=>{
                if(err){
                    cb(null)
                }else{     
                    cb(true)
                }
            })
        }
    }

    async un_set_like(postID , userID , cb){
        let userLikeDecrement = await UserSetter.decrementLike(postID);
        let postLikeDecrement = await PostSetter.decrementLike(postID);
        if(userLikeDecrement === null && postLikeDecrement === null){
            cb(null)
        }else if(userLikeDecrement === null){
            await PostSetter.incrementLike(postID);
        }else if(postLikeDecrement === null){
            await UserSetter.incrementLike(postID);
        }else{
            this.model.Like.findOneAndDelete({liker : userID , post : postID} , (err , result)=>{
                if(err){
                    cb(null)
                }else{
                    cb(true)
                }
            })
        }
    }

}

/*
{
            this.model.Like.findOneAndDelete({liker : userID , post : postID} , (err , result)=>{
                if(err){
                    cb(null)
                }else{
                    cb(true)
                }
            })
        }*/