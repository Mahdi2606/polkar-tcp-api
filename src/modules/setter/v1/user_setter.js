

const controller = require('./../../../controllers/controller');
const UserGetter = require('./../../getter/v1/user_getter')


module.exports = new class UserSetter extends controller {


  async update_user_by_id(id , update){
    let newUpdate = this.model.User.findByIdAndUpdate(id , update , (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    })
    return newUpdate
  }


  async incrementLike(postID){
    let userID = await UserGetter.get_user_id_by_post_id(postID)
    let user = await UserGetter.get_user_by_id(userID);
    let update = await this.update_user_by_id(userID , {likes : user.likes + 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }

  async decrementLike(postID){
    let userID = await UserGetter.get_user_id_by_post_id(postID)
    let user = await UserGetter.get_user_by_id(userID);
    let update = await this.update_user_by_id(userID , {likes : user.likes - 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }




}