const controller = require('./../../../controllers/controller');
const UserGetter = require('./../../getter/v1/user_getter');
const PostGetter = require('./../../getter/v1/post_getter');
module.exports = new class PostSetter extends controller {


  async set_new_post(reqUser,post,cb){
    try {
      let {subject , text , photos , pin , commentMode} = post
      let regex = /#([\w.]+)/g;
      let hashtag = await this.splitWithRegex(regex , text , ' ');
      let user = await UserGetter.get_user_by_id(reqUser._id);
      let userPosts = user.posts;
      let newPost = this.model.Post({
        subject : subject,
        text : text,
        photos : photos,
        pin : pin,
        user : reqUser._id,
        hashtag : hashtag,
        commentMode : commentMode
      })
      newPost.save(async err => {
        if(err){
          cb(null)
        }else{
          userPosts.push(newPost._id)
          let modified = await this.model.User.findOneAndUpdate({_id : reqUser._id} , {posts : userPosts},(err , res)=>{
            if(err || res === null) {
              return null
            }else{
              return res
            }
          })

          if(modified === null){
            cb(null)
          }else{
            cb(newPost)
          }
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  async update_post_by_id(id , update){
    let newUpdate = this.model.Post.findByIdAndUpdate(id , update , (err , result)=>{
      if(err || result === null){
        return null
      }else{
        return result
      }
    })
    return newUpdate
  }

  async incrementLike(postID){
    let post = await PostGetter.get_post_by_id(postID);
    let update = await this.update_post_by_id(postID , {like : post.like + 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }


  async decrementLike(postID){
    let post = await PostGetter.get_post_by_id(postID);
    let update = await this.update_post_by_id(postID , {like : post.like - 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }

  async incrementComment(postID){
    let post = await PostGetter.get_post_by_id(postID);
    let update = await this.update_post_by_id(postID , {commentCount : post.commentCount + 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }


  async decrementComment(postID){
    let post = await PostGetter.get_post_by_id(postID);
    let update = await this.update_post_by_id(postID , {commentCount : post.commentCount - 1});
    if(update === null){
      return null
    }else{
      return update
    }
  }

}
