
const controller = require('./../../../controllers/controller');
const UserGetter = require('./../../getter/v1/user_getter');
const UserSetter = require('./../../setter/v1/user_setter');
module.exports = new class FollowSetter extends controller{


  async set_follow(followerID , followedID , cb){
    let followerUser = await UserGetter.get_user_by_id(followerID)
    let followedUser = await UserGetter.get_user_by_id(followedID)
    let pageMode = followedUser.settings.pageMode
    let newFollow = this.model.Follow({
      follower : followerID,
      followed : followedID,
      status : !pageMode
    })
    followerUser.following.push(followedID)
    followedUser.follower.push(followerID)
    let followerUpdate = await UserSetter.update_user_by_id(followerID , {following : followerUser.following})
    let followedUpdate = await UserSetter.update_user_by_id(followedID , {follower : followedUser.follower})
    if(followedUpdate === null || followerUpdate === null){
      cb(null)
    }else{
      newFollow.save(err=>{
        if(err){
          cb(null)
        }else{
          return cb(true)
        }
      })
    }
  }

  async un_set_follow(followerID , followedID , cb){
    let followerUser = await UserGetter.get_user_by_id(followerID)
    let followedUser = await UserGetter.get_user_by_id(followedID)
    var index = await followedUser.follower.indexOf(followerUser._id)
    followerUser.following = await this.removeItem(followerUser.following,index)
    followedUser.follower = await this.removeItem(followedUser.follower,index)
    let followerUpdate = await UserSetter.update_user_by_id(followerID , {following : followerUser.following})
    let followedUpdate = await UserSetter.update_user_by_id(followedID , {follower : followedUser.follower})
    if(followedUpdate === null || followerUpdate === null){
      cb(null)
    }else{
      this.deleteFollow(followerID , followedID , status =>{
        if(status){
          cb(true)
        }else{
          cb(null)
        }
      });
    }
  }

  async removeItem(items, i){
    let newArray = await items.slice(0, i).concat(items.slice(i+1, items.length))
    return newArray
  }
  
  async deleteFollow(followerID , followedID , cb){
    this.model.Follow.findOneAndDelete({follower : followerID , followed : followedID} , (err , result)=>{
      if(err){
        cb(null)
      }else{
        cb(true)
      }
    })
  }


}