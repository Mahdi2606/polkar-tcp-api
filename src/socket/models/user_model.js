
const controller = require('./../../controllers/controller')

class User extends controller{

    constructor() {
        super();
        this.userRoom;
        this.privateRoom;
        this.username;
        this.name;
        this.id;
        this.io;
    }


    setUser(username){
        this.model.User.findOne({username : username} , (user , err)=>{
            console.log(username)
        })
    }

    async createGroupName(arr) {
        let sort = await arr.sort();
        let room = await sort.join("");
        let PrivateRoom = this.model.PrivateRoom({
            roomID : room,
            chatWith : arr
        });
        return room;
    }


}


module.exports = User;