
const User = require('../../models/user_model')

module.exports = class SocketController{

    follow(data){
        this.io.in(data.room).emit("newFollow", {
            action : 'follow',
            username : data.username,
            profile : data.profile,
            time : data.time,
            message : data.message
        });
    }

    unFollow(data){
        this.io.in(data.room).emit("newUnFollow", {
            action : 'unfollow',
            username : data.username,
            profile : data.profile,
            time : data.time,
            message : data.message
        });
    }

    like(data){
        let emitData = {
            action : 'like',
            username : data.username,
            profile : data.profile,
            time : data.time,
            postID : data.postID,
            message : data.message
        }
        this.io.in(data.room).emit("newLike", emitData);
        this.io.of('/post').in(data.postID).emit('newLike' , emitData)
    }

    dislike(data){
        this.io.in(data.room).emit("newDislike", {
            action : 'dislike',
            username : data.username,
            profile : data.profile,
            time : data.time,
            postID : data.postID,
            message : data.message
        });
    }

    comment(data){
        this.io.in(data.room).emit("newComment", {
            action : 'comment',
            username : data.username,
            profile : data.profile,
            time : data.time,
            postID : data.postID,
            message : data.message,
            text :  data.text
        });
    }

    postComment(room , data){
        console.log(room , data)
        this.io.in(room).emit("newPostComment", {
            action : 'comment',
            username : data.username,
            profile : data.profile,
            time : data.time,
            postID : data.postID,
            message : data.message,
            text :  data.text
        });
    }
}