

const transform = require('../../transforms/transform');


module.exports = class socketUserTransform extends transform{

    transform(item){
        return {
          id : item._id,
          username : item.username,
          profile : item.profile,
        }
    }

}
