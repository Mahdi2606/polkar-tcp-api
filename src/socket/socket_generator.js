


module.exports = class SocketGenerator {


    generateFollow(data){
        return {
            action : 'follow',
            username : data.username,
            profile : data.profile,
            room : data.room,
            message : 'start following you',
            ...this.getTime(),
        }
    }

    generateUnFollow(data){
        return {
            action : 'unfollow',
            username : data.username,
            profile : data.profile,
            room : data.room,
            message : 'stop following you',
            ...this.getTime(),
        }
    }

    generatLike(data){
        return {
            action : 'like',
            username : data.username,
            profile : data.profile,
            room : data.room,
            message : 'like your post',
            postID : data.postID,
            ...this.getTime(),
        }
    }

    generatDislike(data){
        return {
            action : 'dislike',
            username : data.username,
            profile : data.profile,
            room : data.room,
            message : 'dislike your post',
            postID : data.postID,
            ...this.getTime(),
        }
    }
    
    generateComment(data){
        return {
            action : 'comment',
            username : data.username,
            profile : data.profile,
            room : data.room,
            message : 'has inserted a new comment for your post',
            text : data.text,
            postID : data.postID,
            ...this.getTime(),
        }
    }


    getTime(){
        let date = new Date();
        let min = date.getMinutes();
        let hour = date.getHours();

        return {
            time : `${hour}:${min}`
        }
    }


}