
const SocketController = require('./controller/v1/socket_controller')
const UserGetter = require('./../modules/getter/v1/user_getter')
const socketUserTransform = require('./../socket/transforms/sokcet_user_transform')
module.exports = class SocketHandle extends  SocketController{

    constructor(io){
        super();
        this.io = io
        this.client;
        this.connent();
    }

    connent(){
        let io = this.io
        io.of('/').on('connection' , async client => {
            let token = client.handshake.query.token
            let user = await UserGetter.get_user_with_token(token);
            if(!user || user === null){
                client.disconnect();
            }else{
                let socketUser = new socketUserTransform().transform(user)
                client.join(socketUser.id);
                console.log('%s join to the own groupe' , user.username);
                this.client = client


                client.on('postGroup' , data =>{
                    console.log('user joid to ' , data.postID)
                    client.join(data.postID);
                })
    
    
                client.on('levePostGroup' , data => {
                    client.leave(data.postID);
                })
    
                
    
                client.on('disconnect' , ()=>{
                    // var clients = this.io.sockets.adapter.rooms
                    // console.log(clients)
                    console.log('user left')
                })


                client.on('userDisconnect' , data => {
                    client.leave(socketUser.id);
                    console.log(user.username , ' leave app')
                })
    

            }


            
        })
    }
    // socket.disconnect()
    // start(data){
    //     this.client.join(data.id);
    //     this.client.emit('newUser' , `user joid to group : ${data.id}`);
    // }


    emit(data){
        switch (data.action) {
            case 'follow':
                this.follow(data);
                break;
            case 'unfollow':
                this.unFollow(data);
                break;
            case 'like':
                this.like(data);
                break;
            case 'dislike':
                this.dislike(data);
                break;
            case 'comment' :
                this.comment(data);
                break;
            default:
                break;
        }


    }

    postEmit(postRoom , data){
        switch (data.action) {
            case 'comment' :
                this.postComment(postRoom , data);
                break;
            default:
                break;
        }
    }
    
}