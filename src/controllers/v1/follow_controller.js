const FollowGetter = require('./../../modules/getter/v1/follow_getter');
const FollowSetter = require('./../../modules/setter/v1/follow_setter');
const UserGetter = require('./../../modules/getter/v1/user_getter');
const socketGenerator = require('./../../socket/socket_generator')
const ObjectId = require('mongoose').Types.ObjectId;
module.exports = new class followController {


  async follow(req , res){
    let followerID = req.user._id;
    let followedID = await UserGetter.get_user_id_by_username(req.params.username)

    if(followedID !== null){
      // let userExist = await UserGetter.check_user_exist({_id : followedID})
      // if(userExist !== null){
        let exist = await FollowGetter.check_follow_exist(followerID , followedID);
        if(exist === null){
          FollowSetter.set_follow(followerID , followedID , status=>{
            if(status){
              res.json({
                message : 'کاربر دنبال شد',
                status : true,
              })

              let socketData = {
                username : req.user.username,
                profile : req.user.profile,
                room : followedID
              } 
              
              
              let SocketAction = new socketGenerator().generateFollow(socketData)
              global.io.emit(SocketAction);

              return
            }else{
              res.json({
                message : 'خطا در دنبال کردن کاربر',
                status : false
              })
              return
            }
          });
        }else{
          res.json({
            message : 'کاربر از قبل دنبال شده است',
            status : false
          })
          return
        }
      // }else{
      //   res.json({
      //     message : 'کاربری با این ایدی وجود ندارد',
      //     status : false
      //   })
      //   return
      // }
    }else{
      res.json({
        message : 'کاربری با این نام کاربری وجود ندارد',
        status : false
      })
      return
    }

  }

  async unfollow(req , res){

    let followerID = req.user._id;
    let followedID = await UserGetter.get_user_id_by_username(req.params.username)
    // console.log(followedID)
    if(followedID !== null){
      let exist = await FollowGetter.check_follow_exist(followerID , followedID);
      if( exist !== null ){
        FollowSetter.un_set_follow(followerID , followedID , status => {
          if(status){
            res.json({
              message : 'کاربر لغو دنبال شد',
              status : true,
            })

            let socketData = {
              username : req.user.username,
              profile : req.user.profile,
              room : followedID
            } 
            let SocketAction = new socketGenerator().generateUnFollow(socketData)
            global.io.emit(SocketAction);

            return
          }else{
            res.json({
              message : 'خطا در لغو دنبال کردن',
              status : false,
            })
            return
          }
        })
      }else{
        res.json({
          message :  `شما کاربر  ${req.params.username}  را دنبال نکرده اید`,
          status : false,
        })
        return
      }
    }else{
      res.json({
        message : 'کاربری با این نام کاربری وجود ندارد',
        status : false,
      })
      return
    }

  }

  async accept(req , res){
    let followerID = req.user._id;
    let followedID = await UserGetter.get_user_id_by_username(req.params.username)

    let accept = await FollowGetter.check_follow_exist_and_accept(followerID,followedID);
    res.json(accept)

  }
} 