const ObjectId = require('mongoose').Types.ObjectId;


//setters
const PostSetter = require('./../../modules/setter/v1/post_setter');

//getters
const postGetter = require('./../../modules/getter/v1/post_getter');
const userGetter = require('./../../modules/getter/v1/user_getter');

//transforms
const postTransform = require('./../../transforms/v1/posts_transform');
const givePostTransform = require('./../../transforms/v1/give_post_transform');

module.exports = new class PostController {

  async addPost(req , res){
    let post = {
      subject : req.body.subject ? req.body.subject : null , 
      text : req.body.text , 
      photos : req.medias, 
      pin : req.body.pin ,
      commentMode : req.body.commentMode
    }

    PostSetter.set_new_post(req.user,post,response=>{
      if(response === null){
        res.json({
          message : 'خطا در ثبت مطلب 1',
          status : false,
        })
      }else{
        res.json({
          data : response,
          message : 'مطلب با موفقیت ایجاد شد',
          status : true,
        })
      }
    });
  }

  async getAuthUserPost(req , res){
    let userID = req.user._id;
    let page = req.query.page || 1;
    let posts = await postGetter.get_post_by_user_id(page,userID);

    if(posts === null){
      res.json({
        message : 'کاربر پستی ندارد',
        status : false
      })
    }else{

      res.json({
        data : {
          posts : new postTransform().transformCollection(posts.docs),
          limit : posts.limit,
          total : posts.total,
          page : posts.page,
          pages : posts.pages,
        },
        status : true
      })
    }

  }

  async showPost(req , res){
    let post = await postGetter.get_post_by_id(req.params.id)
    res.json({
      data : {
        post : new postTransform().transform(post),
        auth : req.user ? true : false,
        follow : req.follow,
      },
      message : 'پست یافت شد',
      status : true
    })
    return   
  }
  
  async getFollowingUserPosts(req , res){
    let page = req.query.page || 1;
    let following = req.user.following
    let posts = await postGetter.get_following_user_posts(following,page);
    if(posts === null){
      res.json({
        message : 'پستی موجود نیست',
        status : false
      })
      return
    }else{
      res.json({
        data : new postTransform().transformCollection(posts.docs),
        limit : parseInt(posts.limit),
        total : parseInt(posts.total),
        page : parseInt(posts.page),
        pages : parseInt(posts.pages),
        status : true
      })
      return
    }
  }

  async getUsersPosts(req , res){
    let username = req.params.username;
    let page = req.query.page || 1;
    let userID = await userGetter.get_user_id_by_username(username)
    let posts = await postGetter.get_post_by_user_id(page,userID);
    res.json({
      data :  req.pageMode 
              ? req.user && req.follow 
                ? new givePostTransform().transformCollection(posts.docs)
                : false
              : new givePostTransform().transformCollection(posts.docs),
      total: parseInt(posts.total),
      limit: parseInt(posts.limit),
      page: parseInt(posts.page),
      pages: parseInt(posts.pages),
    })
  }

  async searchByHashtag(req , res){
    let hashtag = req.params.hashtag
    let post = await postGetter.search_posts_by_hashtag(hashtag)
    if(post === null){
      res.json({
        status : fslse
      })
      return
    }else{
      res.json({
        data : post,
        status : true
      })
      return
    }
  }

}