
const LikeSetter = require('./../../modules/setter/v1/like_setter');
const LikeGetter = require('./../../modules/getter/v1/like_getter');
const UserGetter = require('./../../modules/getter/v1/user_getter');
const socketGenerator = require('./../../socket/socket_generator');


module.exports = new class LikeController {


    async setLike(req , res) {
        let postID = req.params.id;
        let idValidate = global.ObjectId.isValid(postID);
        if(idValidate){
            let userID = req.user._id;
            let likedID = await UserGetter.get_user_id_by_post_id(postID);
            let exist = await LikeGetter.checkLikeExist(userID , postID);
            if(exist === null){
                LikeSetter.set_like(postID , userID , cb =>{
                    if(cb){
                        res.json({
                            message : 'مطلب لایک شد',
                            status : true,
                        })
                        
                        let socketData = {
                            username : req.user.username,
                            profile : req.user.profile,
                            room : likedID,
                            postID : postID
                          } 
                          let SocketAction = new socketGenerator().generatLike(socketData)
                          global.io.emit(SocketAction);
                        //   global.io.postEmit(SocketAction);
                        return;
                    }else{
                        res.json({
                            message : 'خطا در لایک کردن مطلب',
                            status : false,
                        })
                        return;
                    }
                })
            }else{
                LikeSetter.un_set_like(postID , userID , cb =>{
                    if(cb){
                        res.json({
                            message : 'مطلب لغو لایک شد',
                            status : true,
                        }) 
                        let socketData = {
                            username : req.user.username,
                            profile : req.user.profile,
                            room : likedID,
                            postID : postID
                          } 
                          let SocketAction = new socketGenerator().generatDislike(socketData)
                          global.io.emit(SocketAction);
                        return;
                    }else{
                        res.json({
                            message : 'خطا در لغو لایک مطلب',
                            status : false,
                        })
                        return;
                    }
                })                
            }
        }else{
            res.json({
                message : 'ایدی پست معتبر نیست',
                status : false,
            });
            return
        }
    }


    async checkLike(req , res){
        let postID = req.params.id;
        let idValidate = global.ObjectId.isValid(postID);
        if(idValidate){
            let userID = req.user._id;
            let exist = await LikeGetter.checkLikeExist(userID , postID);
            if(!exist || exist === null){
                res.json({
                    message : 'این پست توسط شما لایک نشده است',
                    liked : false,
                    status : true
                })
                return;
            }else{
                res.json({
                    message : 'این پست توسط شما لایک شده است',
                    liked : true,
                    status : true
                })
                return;
            }
        }else{
            res.json({
                message : 'ایدی پست معتبر نیست',
                status : false,
            });
            return
        }

    }
}