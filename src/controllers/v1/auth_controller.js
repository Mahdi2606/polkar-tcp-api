
const controller = require('./../controller');
const UserGetter = require('../../modules/getter/v1/user_getter');
const loginTransform = require('./../../transforms/v1/login_transform');
const socketUserTransform = require('../../socket/transforms/sokcet_user_transform')
module.exports = new class authController extends controller {

    async register(req , res){

        req.checkBody('name','نام نمیتواند خالی باشد').notEmpty();
        req.checkBody('username','نام کاربری نمیتواند خالی باشد').notEmpty();
        req.checkBody('password','رمز نمیتواند خالی باشد').notEmpty();
        req.checkBody('phone','شماره موبایل نمیتواند خالی باشد').notEmpty();
        req.checkBody('username' , 'نام کاربری نمیتواند کمتر از 8 کاراکتر باشد').isLength({ min: 8 });
        
        if(this.showValidateErrors(req , res)){
            return;
        }
        else{
            try {
                let {name , username , password , phone} = req.body
                let newUser = this.model.User({
                    "name" : name,
                    "username" : username,
                    "password" : password,
                    "phone" : phone, 
                });       
                
                let newSetting = this.model.UserSetting({
                    user : newUser._id
                })

                newUser.settings = newSetting._id

                newSetting.save(err => {
                    if(err){
                        res.json({
                                err : err,
                                message : 'خطا در ثبت کاربر',
                                status : false,
                        })
                        return;
                    }
                    else{
                        newUser.save(err => {
                            if(err){
                                (err.name === 'MongoError' && err.code === 11000)
                                ?   res.json({
                                        message : 'کاربر با این مشخصات موجود است',
                                        status : false,
                                    })
                                :   res.json({
                                        err : err,
                                        message : 'خطا در ثبت کاربر',
                                        status : false,
                                    })
                                
                                return;
                            }
                            else{
                                res.json({
                                        message : 'کاربر با موفقیت ثبت نام شد',
                                        status : true,
                                })
                                return;
                            }
                        })
                    }
                })
            }
            catch (error) {
                console.log(error);
            }
        }

    }

    async login(req , res){
        req.checkBody('username','نام کاربری نمیتواند خالی باشد').notEmpty();
        req.checkBody('password','رمز نمیتواند خالی باشد').notEmpty();
        if(this.showValidateErrors(req , res)){
            return;
        } else {
            try {
                let {username , password} = req.body
                let user = await UserGetter.get_user(username,password);
                if(user !== null){
                    res.json({
                        data : new loginTransform().transform(user),
                        status : true
                    })
                    return
                }else{
                    res.json({
                        data : 'کاربر یافت نشد',
                        status : false
                    })
                    return
                }   
            } catch (error) {
                console.log(error)
            }
        }
    }

}
