
//setter
const UserSetter = require('./../../modules/setter/v1/user_setter');

//getter
const UserGetter = require('./../../modules/getter/v1/user_getter'); 

//transforms
const showCurrentUserTransform = require('./../../transforms/v1/show_current_user_transform');
const userTransform = require('./../../transforms/v1/user_transform');
const giveUserTransform = require('./../../transforms/v1/give_user_transfom')

//validation
const validation = require('./../validation');
module.exports = new class userController extends validation{

  async show_current_user(req , res){
    
    res.json({
      data : new showCurrentUserTransform().transform(req.user),
      status : true
    })
  }

  async updateUser(req , res){
    if(Object.entries(req.body).length === 0){
      res.json({
        message : 'خطا',
        status : false
      })
      return
    }else{


      (req.body.name || req.body.name == '') 
      ? req.checkBody('name','نام نمیتواند خالی باشد').notEmpty()
      : false;

      (req.body.username || req.body.username == '') 
      ? req.checkBody('username','نام کاربری نمیتواند خالی باشد').notEmpty()
      : false;

      (req.body.username || req.body.username == '') 
      ? req.checkBody('username' , 'نام کاربری نمیتواند کمتر از 8 کاراکتر باشد').isLength({ min: 8 })
      : false;

      (req.body.family || req.body.family == '') 
      ? req.checkBody('family','نام خانوادگی نمیتواند خالی باشد').notEmpty()
      : false;

      (req.body.email  || req.body.email == '') 
      ? req.checkBody('email','ایمیل نمیتواند خالی باشد').notEmpty()
      : false;

      (req.body.email) 
      ? req.checkBody('email','فرمت ایمیل صحیح نیست').isEmail()
      : false;

  
      if(this.showValidateErrors(req , res)){
        return
      }else{
        if(req.avatar !== null){
          req.body.profile = req.avatar;
        }
        let userID = req.user._id;
        let idValidate = ObjectId.isValid(userID)
        if(idValidate){
          let update = await UserSetter.update_user_by_id(userID , req.body)
          if(update !== null){
            let newUser = await UserGetter.get_user_by_id(update._id);
            res.json({
              data : new showCurrentUserTransform().transform(newUser),
              message : 'اطلاعات کاربر با موفقیت بروز شد',
              status : true
            })
            return
          }else{
            res.json({
              message : 'خطا در بروزرسانی اطلاعات کاربر',
              status : false
            })
            return
          }
        }else{
          res.json({
            message : 'ایدی کاربر معتبر نیست',
            status : false
          })
          return
        }
      }
    }
  }


  async search(req , res){
    let searchKey = req.params.username;
    let users = await UserGetter.search_users(searchKey);
    if(users === null){
      res.json({
        message : 'کاربری یافت نشد',
        status : false
      })
      return
    }else{
      res.json({
        data : users,
        message : 'جستجو انجام شد',
        status : true
      })
      return
    }
  }

  async getUser(req , res){
    let userID = await UserGetter.get_user_id_by_username(req.params.username)
    let user = await UserGetter.get_user_by_id(userID)

    if(user){
      if(req.follow){
        res.json({
          data : new userTransform().transform(user , true),
          status : true
        })
        return
      }else{
        res.json({
          data : new userTransform().transform(user , false),
          status : true
        })
        return
      }
    }else{
      res.json({
        message : 'کاربری یافت نشد',
        status : false,
      })
    }
  }

  async giveUser(req , res) {
    let userID = await UserGetter.get_user_id_by_username(req.params.username)
    let user = await UserGetter.get_user_by_id(userID)

    let auth = req.user ? true : false;
    let follow = req.follow;
    console.log(follow)
    let pageMode = req.pageMode;
    let privateUserID = req.privateUserID;

      res.json({
        data : new giveUserTransform().transform(user , auth , follow , pageMode),
        message : 'user find',
        status : true
      })



  }

  
} 