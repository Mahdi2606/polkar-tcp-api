const CommentSetter = require('./../../modules/setter/v1/comment_setter');
const CommentGetter = require('./../../modules/getter/v1/comment_getter');
const UserGetter = require('./../../modules/getter/v1/user_getter')
const socketGenerator = require('./../../socket/socket_generator')
//transforms
const commentTransform = require('./../../transforms/v1/comment_transform');
module.exports = new class commentController {



  async addComment(req , res){
    let {text} = req.body;
    let post = req.params.id;
    let user = req.user._id;
    let idValidate = ObjectId.isValid(post)
    if(idValidate){
      CommentSetter.set_comment(post , user , text , async status => {
        if(status){
          res.json({
            message : 'دیدگاه با موفقیت ثبت شد',
            status : true,
          })

          let postUserID = await UserGetter.get_user_id_by_post_id(post);
          let socketData = {
            username : req.user.username,
            profile : req.user.profile,
            room : postUserID,
            postID : post,
            text : text
          } 
          let SocketAction = new socketGenerator().generateComment(socketData)
          global.io.emit(SocketAction);
          global.io.postEmit(post , SocketAction);
          return
        }else{
          res.json({
            message : 'خطا در ثبت دیدگاه',
            status : false,
          })
          return
        }
      })
    }else{
      res.json({
        message : 'ایدی مطلب معتبر نیست',
        status : false,
      })
      return
    }
    
  }

  async getLastComment(req , res){
    let username = req.user.username;
    let postID = req.params.id;
    let userID = await UserGetter.get_user_id_by_username(username);
    let lastComment = await CommentGetter.getLastComment(postID , userID);
    if(lastComment === null){
      res.json({
        message : 'کامنتی یافت نشد',
        status : false
      })
      return
    } else{
      res.json({
        message : 'کامنت یافت شد',
        data : new commentTransform().transform(lastComment),
        status : true
      })
      return
    }
  }
  
  async showPostComment(req , res){
    let page = req.query.page || 1;
    let postID = req.params.id;
    let idValidate = ObjectId.isValid(postID)
    if(idValidate){
      let comments = await CommentGetter.get_post_comments(postID,page);
      let data = new commentTransform().transformCollection(comments.docs)
      res.json({
        data : data,
        total: comments.total,
        limit: comments.limit,
        page: comments.page,
        pages: comments.pages,
        status : true 
      })
      return
    }else{
      res.json({
        message : 'ایدی مطلب معتبر نیست',
        status : false
      })
      return
    }
  }



} 