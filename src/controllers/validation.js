
module.exports = class validation {
    
    showValidateErrors(req , res){
        const errors = req.validationErrors()
        if(errors) {
            res.json({
                error : errors.map((error) => {
                    return {
                        field : error.param,
                        message : error.msg
                    }
                }),
                status : false
            })
            return true
        }
    }

}