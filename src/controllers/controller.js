const User = require('./../models/User');
const Post = require('./../models/Post');
const Follow = require('./../models/Follow');
const UserSetting = require('./../models/UserSetting');
const Comment = require('./../models/Comment');
const PrivateRoom = require('./../models/PrivateRoom');
const Like = require('./../models/Like');
module.exports = class controller {
    
    constructor() {
        this.model = {
            User,
            UserSetting,
            Post,
            Follow,
            Comment,
            PrivateRoom,
            Like
        }
    }
    
    async splitWithRegex(regex = RegExp , string = String , joinChar = ',' , array = false){
        let split = string.match(regex);
        if(split !== null){
            if(array){
                return split
            }else{
                let str = split.join(joinChar)
                return str
            }
        }else{
            return '';
        }
    }

    showValidateErrors(req , res){
        const errors = req.validationErrors()
        if(errors) {
            res.json({
                error : errors.map((error) => {
                    return {
                        field : error.param,
                        message : error.msg
                    }
                }),
                status : false
            })
            return true
        }
    }

}