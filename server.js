
const express = require('express');
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io").listen(http);
const socketHandle = require('./src/socket/socket_handle')
const route = require('./src/routes/index');

//socketIO
 global.io = new socketHandle(io);
//socketIO

global.config = require('./src/config');
global.ObjectId = require('mongoose').Types.ObjectId;
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose'); 
mongoose.connect('mongodb://127.0.0.1:27017/polKar' , { useNewUrlParser: true, useCreateIndex : true });
mongoose.Promise = global.Promise;
app.use('/files' , express.static('files'))
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods", "*");
    next();
  });
app.use(bodyParser.urlencoded({ extended : false , limit: '50mb' }));
app.use(bodyParser.json({ type : 'application/json' , limit: '50mb' }));
app.use(expressValidator());
app.use('/' , route)





http.listen(config.port , ()=> console.log(`PolKar api running on port %s` , config.port))


